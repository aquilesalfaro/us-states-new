//
//  StateDetailViewController.
//  sc00-US-States
//
//  Created by user on 11/1/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import "StateDetailViewController.h"
//#import "States.h"

@interface StateDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *stateFlagImage;
@property (weak, nonatomic) IBOutlet UILabel *detailStateMotto;
@property (weak, nonatomic) IBOutlet UILabel *stateDetailCapital;
@property (weak, nonatomic) IBOutlet UILabel *stateDetailPopulation;
@property (weak, nonatomic) IBOutlet UILabel *stateDetailName;
@property (weak, nonatomic) IBOutlet UILabel *stateDetailBird;
@property (weak, nonatomic) IBOutlet UIImageView *stateDetailBirdImage;



@end

@implementation StateDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.stateDetailCapital.text = self.myState.capital;
    self.stateFlagImage.image = self.myState.flag;
    self.stateDetailName.text = self.myState.name;
    self.stateDetailBird.text = self.myState.birdName;
    self.stateDetailBirdImage.image = self.myState.bird;
    self.stateDetailPopulation.text = self.myState.population;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
